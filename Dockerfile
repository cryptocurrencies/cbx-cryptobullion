# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
# Checkout latest Sourcecode
RUN git clone https://github.com/cryptogenicbonds/CryptoBullion-CBX.git /opt/cryptobullion && \
    cd /opt/cryptobullion && \
    git checkout tags/2.4.4 && \
    cd /opt/cryptobullion/src/leveldb/ && make libleveldb.a libmemenv.a && \
    cd /opt/cryptobullion/src/miniupnpc && make && cd .. && \
    make -j2 -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r cryptobullion && useradd -r -m -g cryptobullion cryptobullion
RUN mkdir /data
RUN chown cryptobullion:cryptobullion /data
COPY --from=build /opt/cryptobullion/src/cryptobulliond /usr/local/bin/
USER cryptobullion
VOLUME /data
EXPOSE 7695 8395
CMD ["/usr/local/bin/cryptobulliond", "-datadir=/data", "-conf=/data/CryptoBullion.conf", "-server", "-txindex", "-printtoconsole"]